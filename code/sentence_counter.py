import argparse
parser = argparse.ArgumentParser()
parser.add_argument("echo")
args = parser.parse_args()

with open(args.echo, "r") as f:
	text = f.readlines()

print("文:", len(text))

words = []
for s in text:
	words += s.split()

print("トークン:", len(words))

print("語彙:", len(set(words)))
