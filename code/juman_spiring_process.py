import jaconv
from pyknp import Jumanpp

juman = Jumanpp()

def owakati(sentence):
	res = juman.analysis(jaconv.h2z(sentence, digit=True, ascii=True))
	return " ".join([str(m.midasi) for m in res.mrph_list()]).replace("　", "")


def main():
	source = []
	with open("./spiring_data_2004_2016.txt", "r") as f:
		text = f.readline()
		text = text.replace(" ", "").replace("&emsp;", "")
		count = 0
		while text:
			print(count)
			source.append("[CLS] " + owakati(text[:-1]) + " [SEP]")
			count += 1
			text = f.readline()

	with open("./juman_source.txt", "w") as f:
		f.write("\n".join(source))


if __name__ == "__main__":
	main()
