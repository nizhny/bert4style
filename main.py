from comet_ml import Experiment
import torch
import torch.nn as nn
import torch.nn.functional as F
from pytorch_pretrained_bert import BertModel, BertTokenizer
from torch.nn.utils.rnn import pad_packed_sequence, pack_padded_sequence
import adabound
import random
import sklearn
from nltk import bleu_score
from sklearn.utils import shuffle
import warnings
import random
warnings.filterwarnings('ignore')

# Add the following code anywhere in your machine learning file
experiment = Experiment(api_key="4as2djQjw8AFMMAemhjuGd7DJ",
                        project_name="bert4styletransfer", workspace="juravrik")
experiment.add_tags(["100000", "1layerLSTM", "noDropout"])

#set_name("100000data")

PAD = 0
UNK = 1
CLS = 2
SEP = 3

bertpath = "../../BERT_juman"
device = 'cuda' if torch.cuda.is_available() else 'cpu'

tokenizer = BertTokenizer.from_pretrained(bertpath, do_lower_case=False)
bert_trained = BertModel.from_pretrained(bertpath)
bert_trained = bert_trained.to(device)


def clean(sentence):
    sentence = tokenizer.tokenize(sentence)
    if len(sentence) > 128:
        sentence = sentence[:127] + ['[SEP]']
    sentence = tokenizer.convert_tokens_to_ids(sentence)
    return sentence


text = []
with open("../dara/juman_source.txt") as f:
    text.append(f.readline())
    count = 1
    while f:
        if count > 1000000:
            break
        text.append(f.readline())
        count += 1

# text = text.split("\n")

source = [s for s in random.sample(text, 100000)]

with open("../output/source_100000.txt", "w") as f:
    f.write("\n".join(source))

source = [clean(s) for s  in source]

with open("../data/juman_reimu.csv") as f:
    text = f.read()

text = text.split("\n")
valid = [clean(s) for s in text]


class DataLoader(object):
    def __init__(self, X, batch_size, bert_model):
        self.data = X
        self.batch_size = batch_size
        self.reset()
        self.bert_model = bert_model

    def reset(self):
        self.data = shuffle(self.data)
        self.start_index = 0

    def __iter__(self):
        return self

    def __next__(self):
        if self.start_index >= len(self.data):
            self.reset()
            raise StopIteration()

        X = self.data[self.start_index:self.start_index + self.batch_size]
        max_seq = max([len(x) for x in X])
        X = [x + [PAD for s in range(max_seq - len(x))] for x in X]
        X = torch.tensor(X, dtype=torch.long, device=device)

        with torch.no_grad():
            batch_embed, _ = self.bert_model(
                X, output_all_encoded_layers=False)

        batch_X = batch_embed[:, 0, :]
        batch_Y = X
	# ポインタを更新する
        self.start_index += self.batch_size
        return batch_X, batch_Y, max_seq


class Decoder(nn.Module):
    def __init__(self, n_embed_dim, n_hidden_dim, vovab_size):
        """
        :param hidden_size: int, 隠れ層のユニット数
        :param output_size: int, 出力言語の語彙数
        :param dropout: float, ドロップアウト率
        """
        super(Decoder, self).__init__()
        self.hidden_size = n_hidden_dim
        self.output_size = vovab_size

        self.embedding = nn.Embedding(vovab_size, n_embed_dim, padding_idx=PAD)
        self.lstm = nn.LSTM(n_embed_dim, n_hidden_dim, batch_first=True)
        self.out = nn.Linear(n_hidden_dim, vovab_size)

    def forward(self, seqs, hidden):
        """
        :param seqs: tensor, 入力のバッチ, size=(1, batch_size)
        :param hidden: tensor, 隠れ状態の初期値, Noneの場合は0で初期化される
        :return output: tensor, Decoderの出力, size=(1, batch_size, output_size)
        :return hidden: tensor, Decoderの隠れ状態, size=(1, batch_size, hidden_size)
        """
        emb = self.embedding(seqs)
        # print(emb.shape)

        output, hidden = self.lstm(emb, hidden)
        # print(output.shape)
        output = self.out(output)
        return output, hidden


mce = nn.CrossEntropyLoss(reduction='sum', ignore_index=0)


def calc_bleu(refs, hyps):
    """
    BLEUスコアを計算する関数
    :param refs: list, 参照訳。単語のリストのリスト (例： [['I', 'have', 'a', 'pen'], ...])
    :param hyps: list, モデルの生成した訳。単語のリストのリスト (例： [['I', 'have', 'a', 'pen'], ...])
    :return: float, BLEUスコア(0~100)
    """
    refs = [[ref[:ref.index(SEP)]] if SEP in ref else ref for ref in refs]
    hyps = [hyp[:hyp.index(SEP)] if SEP in hyp else hyp for hyp in hyps]

    return 100 * bleu_score.corpus_bleu(refs, hyps)


n_embed_dim = 256
n_hidden_dim = 768
vocab_size = len(tokenizer.vocab)
batch_size = 64
epoch = 100
lr = 1e-3

model_args = {
    'n_embed_dim': n_embed_dim,
    'n_hidden_dim': n_hidden_dim,
    'vovab_size': vocab_size
}

params = {
    'n_embed_dim': n_embed_dim,
    'n_hidden_dim': n_hidden_dim,
    'vocab_size': vocab_size,
    'batch_size': batch_size,
    'epoch': epoch,
    'lr': lr
}

experiment.log_parameters(params)

model = Decoder(**model_args).to(device)
#model = torch.nn.DataParallel(model)
# optimizer = adabound.AdaBound(model.parameters(), lr=params['lr'],
# final_lr=params['final_lr'])
optimizer = torch.optim.Adam(model.parameters(), lr=lr)

best_valid_bleu = 0.

train_loader = DataLoader(source, params['batch_size'], bert_trained)
valid_loader = DataLoader(valid, params['batch_size'], bert_trained)

with experiment.train():

    for epoch in range(params['epoch'] + 1):
        print(f"epoch{epoch} start")
        train_loss = 0.
        train_bleu = 0.

        for X, y, l in train_loader:
            model.train()
            # x = X.reshape(1, -1, 768).contiguous()
            # use_teacher_forcing = True
            # random.random() < (0.8)**epoch

            # if use_teacher_forcing:
            #    pred_y = model(x, x.shape[1], l, y, use_teacher_forcing=True)
            # else:
            #    pred_y = model(x, x.shape[1], l)

            hidden = X.view(1, -1, 768).contiguous()
            hidden = (hidden, hidden)

            pred, _ = model(y, hidden)
            loss = mce(pred[:, :-1].contiguous().view(-1,
                       pred.shape[2]), y[:, 1:].contiguous().view(-1))

            optimizer.zero_grad()
            loss.backward()
            optimizer.step()

            train_loss += loss.item()
            train_bleu += calc_bleu(y.contiguous().cpu().tolist(),
                                    pred.max(dim=-1)[1].cpu().tolist())

        total_train_loss = train_loss / len(train_loader.data)
        total_train_bleu = train_bleu / len(train_loader.data)

        print(f"epooch{epoch}'s train_loss:{train_loss}")
        print(f"epooch{epoch}'s train_bleu:{train_bleu}")

        experiment.log_metric('train_loss', total_train_loss, step=epoch)
        experiment.log_metric('train_bleu', total_train_bleu, step=epoch)

        valid_loss = 0.
        valid_bleu = 0.
        valid_text = []
        for X, y, l in valid_loader:
            model.eval()
            # x = X.reshape(1, -1, 768).contiguous()
            # pred_y = model(x, x.shape[1], l)

            # print("sample: "," ".join(tokenizer.convert_ids_to_tokens(pred_y.max(dim=-1)[1].data.cpu().numpy().T.tolist()[0])))
            # loss = criterion(pred_y, y.view(-1))

            hidden = X.view(1, -1, 768).contiguous()

            decoder_input = torch.tensor(
                [CLS] * hidden.shape[1], dtype=torch.long, device=device).view(-1, 1)
            decoder_outputs = torch.zeros(
    hidden.shape[1], 1, len(
        tokenizer.vocab), device=device)

            hidden = (hidden, hidden)

            for i in range(l):
                decoder_output, hidden = model(decoder_input, hidden)
                decoder_outputs = torch.cat(
                    [decoder_outputs, decoder_output], dim=1)
                decoder_input = decoder_output.max(-1)[1]

            for p in decoder_outputs.max(-1)[1]:
                valid_text.append(
    " ".join(
        tokenizer.convert_ids_to_tokens(
            p.cpu().tolist())))

            #pred = decoder_outputs.max(-1)[1]
            loss = mce(decoder_outputs[:, 1:-1].contiguous().view(-1, decoder_outputs.shape[2]), y[:, 1:].contiguous().view(-1))
            valid_loss += loss.item()
            valid_bleu += calc_bleu(y.contiguous().cpu().tolist(),
                                    decoder_outputs.max(dim=-1)[1].cpu().tolist())


        total_valid_loss = valid_loss / len(valid_loader.data)
        total_valid_bleu = valid_bleu / len(valid_loader.data)

        print(f"epooch{epoch}'s valid_loss:{valid_loss}")
        print(f"epooch{epoch}'s valid_bleu:{valid_bleu}")

        experiment.log_metric('valid_loss', total_valid_loss, step = epoch)
        experiment.log_metric('valid_bleu', total_valid_bleu, step = epoch)

        if valid_bleu > best_valid_bleu:
            print("update on", epoch)
            ckpt=model.state_dict()
            torch.save(ckpt, "./model/forStyle.pth")
            best_valid_bleu=valid_bleu

            with open("../output/valid_style.txt", "w") as f:
                f.write("\n".join(valid_text))
